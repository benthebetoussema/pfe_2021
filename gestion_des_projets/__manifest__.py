# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Gestion des Projet',
    'version': '1.0',
    "author": "Oussema ben thebet",
    'category': '',
    'summary': '',
    'sequence': 1,

    'description': "C'est un Module d'organisation de gestion du projet ",

    'depends': ['base','project','hr_timesheet'],
    'data': [
        'views/project.xml',
        'report/report_timesheets.xml',
        'report/timesheet_pdf.xml',
        'wizard/timesheet_wizard.xml',

    ],
    'images': ['gestion_des_projets/static/img/icon.png'],
    'installable': True,
    'application': True,
    "active": True,
    'auto_install': False,
}
