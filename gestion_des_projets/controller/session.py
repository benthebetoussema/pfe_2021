# -*- coding: utf-8 -*-

import logging

try:
    import xlwt
except ImportError:
    xlwt = None


from odoo import http
from odoo.addons.web.controllers.main import  Session
from odoo.http import request, serialize_exception as _serialize_exception
_logger = logging.getLogger(__name__)
class Session(Session):

    @http.route('/web/session/get_session_info', type='json', auth="none",cors="*")
    def get_session_info(self):
        res = super(Session, self).get_session_info()
        return res

    @http.route('/web/session/authenticate', type='json', auth="none",cors="*")
    def authenticate(self, db, login, password, base_location=None):
        print('hello world')
        res = super(Session,self).authenticate(db, login, password)
        return res