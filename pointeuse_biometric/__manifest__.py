{
    "name" : "Pointeuse des Utilisateurs",
    "version" : "1.0",
    "author" : "Oussema Ben Thebet",
    "category" : "HR",
    "website" : "",
    "description": "Module pour les présences des Utilisateurs.",
    'license': 'AGPL-3',
    "depends" : ["base","hr","hr_attendance"],
    "data" : [
				"views/biometric_machine_view.xml",
				"secuirty/res_groups.xml",
				"secuirty/ir.model.access.csv"
			],
	'img': ['static/img/zk_screenshot.jpg'],
    "active": True,
    "installable": True,
}
